public class DelayedCourutine {
    public float Seconds { get; set; }
    public IEnumerator Courutine { get; set; }
 
    public static DelayedCourutine CreateDelayedCourutine(float seconds, IEnumerator cour) {
        return new DelayedCourutine() {
            Courutine = cour,
            Seconds = seconds
        };
    }
}