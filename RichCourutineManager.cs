public class RichCourutineManager {
    private MonoBehaviour _context;
 
    public RichCourutineManager(MonoBehaviour context) {
        _context = context;
    }
 
    private RichCourutineManager() {}
 
    public void RunCourutines(params IEnumerator[] courutines) {
        var queue = FromArrToQueue(courutines);
        RunDelayedCourutines(queue);
    }
 
    public void RunDelayedCourutines(Queue<DelayedCourutine> delayedCourutines) {
        _context.StartCoroutine(RunPrivate(delayedCourutines));
    }
 
    public void RepeatCourutine(int times, IEnumerator courutine) {
        var queue = new Queue<DelayedCourutine>(times);
        for (int i = times; i > 0; i--) {
            queue.Enqueue(DelayedCourutine.CreateDelayedCourutine(0f, courutine));
        }
        RunDelayedCourutines(queue);
    }
 
    public void RepeatCourutines(int times, params IEnumerator[] courutines) {
        var queueLength = courutines.Length*times;
        var queue = new Queue<DelayedCourutine>(queueLength);
        
        for (var i = 0; i < queueLength; i++) {
            var index = i%queueLength;
            var delayedCourutine = DelayedCourutine.CreateDelayedCourutine(0f, courutines[index]);
            queue.Enqueue(delayedCourutine);
        }
 
        RunDelayedCourutines(queue);
    }
 
    public void RunSimultaneously(params IEnumerator[] courutines) {
        var queue = new Queue<IEnumerator>(courutines.Length);
        foreach (var courutine in courutines) {
            queue.Enqueue(courutine);
        }
        _context.StartCoroutine(RunPrivateSim(queue));
    }
 
    private IEnumerator RunPrivate(Queue<DelayedCourutine> cours) {
        while (cours.Count != 0) {
            var delCour = cours.Dequeue();
            var delay = delCour.Seconds;
            yield return _context.StartCoroutine(delCour.Courutine);
            yield return new WaitForSeconds(delay);
        }
    }
 
    private IEnumerator RunPrivateSim(Queue<IEnumerator> cours) {
        while (cours.Count != 0) {
            var courutine = cours.Dequeue();
            _context.StartCoroutine(courutine);
            yield return null;
        }
    }
 
    private Queue<DelayedCourutine> FromArrToQueue(IEnumerator[] courutines) {
        var queue = new Queue<DelayedCourutine>(courutines.Length);
        
        foreach (var courutine in courutines) {
            var delayedCourutine = DelayedCourutine.CreateDelayedCourutine(0f, courutine);
            queue.Enqueue(delayedCourutine);
        }
 
        return queue;
    }
}