//общий класс для разных видов оружия. 

public abstract class Weapon : MonoBehaviour {
    protected Character _weaponOwner;
    public virtual string Name { get; set; }
    protected virtual float ShootingDelay { get; set; }
    public virtual int Damage { get; set; }
    public virtual float ProjectileSpeed { get; set; }

    protected bool InDelay = false;

    public virtual bool CanShoot {
        get { return !InDelay; }
    }

    public virtual void Shoot() {
        if (!InDelay) {
            DelayWeapon();
            StartProjectile();
        }
    }

    void Start() {
        _weaponOwner = GetComponent<Character>();
    }

    /// <summary>
    /// You need to define this in concrete class so it will know which projectile to use
    /// </summary>
    /// <returns>name of prefab in resources</returns>
    public abstract string GetProjectileName();

    /// <summary>
    /// You need to define this in concrete class so weapon will be able calculate coords of projectile starting point
    /// </summary>
    /// <returns>Displamecent in coords from character + weapon</returns>
    public abstract Vector2 GetProjectileDisplacement();

    public abstract Vector2 GetAttackPos();

    private void StartProjectile() {
        var projectileResource = Resources.Load<GameObject>(GetProjectileName());
        var projectilePos = new Vector3(GetAttackPos().x,
            GetAttackPos().y, 0);
        var projectileObj = (GameObject)Instantiate(projectileResource, projectilePos, Quaternion.identity);

        var projectile = projectileObj.GetComponent<Projectile>();
        projectile.Speed = ProjectileSpeed;
        projectile.Damage = Damage;
    }

    private void DelayWeapon() {
        InDelay = true;
        StartCoroutine(RenewWeapon());  
    }

    private IEnumerator RenewWeapon() {
        yield return new WaitForSeconds(ShootingDelay);
        InDelay = false;
    }
}