//скрипт который будет фейдить текст с определенной скоростью (вешается на игровой объект, который имеет компонент TextMesh)
[RequireComponent(typeof(TextMesh))]
public class FadingText : MonoBehaviour {
    public float Speed;

    private TextMesh _textMesh;

    void Start() {
        _textMesh = GetComponent<TextMesh>();
    }

    public void Show() {
        if (!TextVisible()) {
            var c = _textMesh.color;
            _textMesh.color = new Color(c.r, c.g, c.b, 1);
        }
    }

    public void Hide() {
        var c = _textMesh.color;
        _textMesh.color = new Color(c.r, c.g, c.b, 0f);
    }

    public void Fade(Action<string> onFade) {
        StartCoroutine(_Fade(onFade));
    }
        
    public void SetText(string text) {
        _textMesh.text = text;
    }

    private IEnumerator _Fade(Action<string> onFade) {
        while (TextVisible()) {
            FadeTextBy(0.01f * Speed);
            yield return null;
        }
        if(onFade != null)
            onFade(_textMesh.text);
    }

    private void FadeTextBy(float amount) {
        var tColor = _textMesh.color;
        _textMesh.color = new Color(tColor.r, tColor.g, tColor.b, tColor.a - amount);
    }

    private bool TextVisible() {
        return _textMesh.color.a > 0f;
    }
}