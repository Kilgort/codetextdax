public class Character : MonoBehaviour {
    public enum Direction {
        Left = -1,
        Right = 1,
    }

    public int HitPoints = 5;

    private Weapon _weapon;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;

    public float Speed = 5f;

    public bool IsDead {
        get { return HitPoints <= 0; }
    }

    public void Move(float moveAxis) {
        if (moveAxis != 0) {
            var direction = moveAxis > 0 ? Direction.Right : Direction.Left;
            var dir = (int) direction;
            rigidbody2D.velocity = new Vector2(Speed*dir, rigidbody2D.velocity.y);
            transform.localScale = new Vector3(dir*(-1), transform.lossyScale.y, transform.lossyScale.z);
        }
        _animator.SetFloat("Speed", Math.Abs(moveAxis));
    }

    public void Shoot() {
        if (_weapon != null && _weapon.CanShoot) {
            Animate("Shooting");
            _weapon.Shoot(); //we delegate shooting to weapon class
        }
    }

    public void GetHit(int amount) {
        if (!IsDead) {
            Animate("Hit"); //TODO: add hit anim
            //HitPoints -= amount; //immortal now
        }
    }

    public void ChangeWeapon(string weaponName) {
        Destroy(_weapon);
        _weapon = null;
        _weapon = (Weapon) gameObject.AddComponent(weaponName);
    }

    private void Start() {
        _weapon = (Weapon) gameObject.AddComponent("Shotgun"); //shotgun is default weapon for now
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Animate(string animationName) {
        StartCoroutine(animationName + "Anim");
    }

    private IEnumerator ShootingAnim() {
        _animator.SetBool("Shooting", true);
        yield return new WaitForSeconds(0.2f);
        _animator.SetBool("Shooting", false);
    }

    private IEnumerator HitAnim() {
        _spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.5f);
        _spriteRenderer.color = Color.white;
    }
}
